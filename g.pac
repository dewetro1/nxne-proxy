function FindProxyForURL(url, host) {

    // *******************************************
    // Variables that define the proxy fail-over order
    // *******************************************
	var nbsalconproxy="PROXY proxy.alconnet.com:8580; PROXY na-useh-proxy.na.novartis.net:2011; PROXY nibr-proxy.global.nibr.novartis.net:2011";
	var chbsproxy="PROXY chbs-proxy01.nibr.novartis.net:2011; PROXY chbs-proxy02.nibr.novartis.net:2011; PROXY nibr-proxy.global.nibr.novartis.net:2011";
	var sgriproxy="PROXY sgdc-proxy.ap.novartis.net:2011; PROXY chbs-proxy01.nibr.novartis.net:2011; PROXY nibr-proxy.global.nibr.novartis.net:2011";
	var uscaproxy="PROXY usca-proxy01.nibr.novartis.net:2011; PROXY usca-proxy02.nibr.novartis.net:2011; PROXY nibr-proxy.global.nibr.novartis.net:2011";
	var usemproxy="PROXY usem-proxy01.nibr.novartis.net:2011; PROXY usem-proxy02.nibr.novartis.net:2011; PROXY nibr-proxy.global.nibr.novartis.net:2011";
	var nibrproxy="PROXY nibr-proxy.global.nibr.novartis.net:2011; DIRECT";
	var noproxy="DIRECT";

    // *******************************************
	// The function below is intended to grab the active IPv4 IP address, but due to differences in OS and browser implementation of the PAC functions, this does
	// not always work, and may return the localhost or IPv6 address, or potentially something else. This is especially problematic with Mac OS X and Linux systems
    // *******************************************
	var NovartisIP = myIpAddress();

    // *******************************************
    // Web hosts that go to specific proxy servers
    // *******************************************

    // Alcon proxy
    if	(shExpMatch(host, "*.livemeeting.com") 				&&	//Livemeeting fix for Alcon
   		(isInNet(NovartisIP, "10.0.0.0", "255.254.0.0") 	||
		 isInNet(NovartisIP, "161.61.0.0", "255.255.0.0"))
		)
		return nbsalconproxy;
    // Basel proxy
    if	(shExpMatch(host, "rp1.023d.dedicated.lync.com")	||	//Lync Reverse Proxy: Dublin
    	 shExpMatch(host, "rp2.023d.dedicated.lync.com")	||	//Lync Reverse Proxy: Amsterdam
		 shExpMatch(host, "wac.023d.dedicated.lync.com")	||	//Lync Reverse Proxy: Dublin
		 shExpMatch(host, "tsa.quovadisglobal.com")			||	//DoXign Time Stamp: Zurich
		 shExpMatch(host, "tsa01.quovadisglobal.com")		||	//DoXign Time Stamp: Zurich
		 shExpMatch(host, "am3rp2.023d.dedicated.lync.com")		//Lync Reverse Proxy: Amsterdam
		)
		return chbsproxy;
    // Cambridge proxy
    if	(shExpMatch(host, "rp3.023d.dedicated.lync.com")	||	//Lync Reverse Proxy: Chicago
		 shExpMatch(host, "rp4.023d.dedicated.lync.com")	||	//Lync Reverse Proxy: San Antonio
		 shExpMatch(host, "was2.023d.dedicated.lync.com")	||	//Lync Reverse Proxy: Chicago
		 shExpMatch(host, "tsa1.digistamp.com")				||	//DoXign Time Stamp: Dallas
		 shExpMatch(host, "tsa2.digistamp.com")				||	//DoXign Time Stamp: Chicago
		 shExpMatch(host, "tsa11.digistamp.com")			||	//DoXign Time Stamp: Dallas
		 shExpMatch(host, "tsa12.digistamp.com")			||	//DoXign Time Stamp: Chicago
		 shExpMatch(host, "communities.nibr.novartis.net")		//JIVE Redirect
		)
		return uscaproxy;
    // Singapore proxy
    if	(shExpMatch(host, "rp5.023d.dedicated.lync.com")	||	//Lync Reverse Proxy: Hong Kong
		 shExpMatch(host, "was3.023d.dedicated.lync.com")	||	//Lync Reverse Proxy: Singapore
	 	 shExpMatch(host, "rp6.023d.dedicated.lync.com") 		//Lync Reverse Proxy: Singapore
		)
		return sgriproxy;
    // Emeryville proxy
    if	((shExpMatch(host, "*google*")						||	//Google
		 dnsDomainIs(host, ".gstatic.com")					||
		 dnsDomainIs(host, "facebook.com")					||	//Facebook
		 dnsDomainIs(host, ".facebook.com")					||
		 dnsDomainIs(host, "kaltura.com")					||	//Kaltura
		 dnsDomainIs(host, ".kaltura.com")					||
		 dnsDomainIs(host, "youtube.com")					||	//YouTube
		 dnsDomainIs(host, ".youtube.com")					||
		 dnsDomainIs(host, ".ytimg.com")					||
		 dnsDomainIs(host, ".slideshare.net")				||	//SlideShare
		 dnsDomainIs(host, "novartis.onbmc.com")			||	//NBS Remedy On Demand
		 dnsDomainIs(host, "twitter.com")					||	//Twitter
		 dnsDomainIs(host, ".twitter.com")					||
		 dnsDomainIs(host, ".twimg.com")
		) &&
		(
		 isInNet(NovartisIP, "10.132.24.0", "255.255.248.0")	||
		 isInNet(NovartisIP, "10.132.32.0", "255.255.240.0")	||
		 isInNet(NovartisIP, "10.132.48.0", "255.255.255.0")	||
		 isInNet(NovartisIP, "10.132.75.0", "255.255.255.0")	||
		 isInNet(NovartisIP, "10.132.76.0", "255.255.252.0")	||
		 isInNet(NovartisIP, "10.132.80.0", "255.255.240.0")	||
		 isInNet(NovartisIP, "10.132.96.0", "255.255.248.0")	||
		 isInNet(NovartisIP, "10.132.104.0", "255.255.254.0")	||
		 isInNet(NovartisIP, "10.132.143.0", "255.255.255.0")	||
		 isInNet(NovartisIP, "10.132.144.0", "255.255.240.0")	||
		 isInNet(NovartisIP, "10.132.234.0", "255.255.254.0")	||
		 isInNet(NovartisIP, "10.132.236.0", "255.255.252.0")	||
		 isInNet(NovartisIP, "10.132.240.0", "255.255.248.0")	||
		 isInNet(NovartisIP, "10.133.96.0", "255.255.224.0")	||
		 isInNet(NovartisIP, "10.133.208.0", "255.255.240.0")	||
		 isInNet(NovartisIP, "10.133.224.0", "255.255.224.0")	||
		 isInNet(NovartisIP, "10.135.192.0", "255.255.248.0")	||
		 isInNet(NovartisIP, "10.170.0.0", "255.254.0.0")		||
		 isInNet(NovartisIP, "10.232.0.0", "255.252.0.0")		||
		 isInNet(NovartisIP, "86.116.8.0", "255.255.254.0")		||
		 isInNet(NovartisIP, "86.116.16.0", "255.255.254.0")	||
		 isInNet(NovartisIP, "86.116.24.0", "255.255.255.0")	||
		 isInNet(NovartisIP, "86.116.28.0", "255.255.252.0")	||
		 isInNet(NovartisIP, "86.116.32.0", "255.255.255.0")	||
		 isInNet(NovartisIP, "86.116.34.0", "255.255.254.0")	||
		 isInNet(NovartisIP, "86.116.36.0", "255.255.252.0")	||
		 isInNet(NovartisIP, "86.116.40.0", "255.255.252.0")	||
		 isInNet(NovartisIP, "86.116.44.0", "255.255.254.0")	||
		 isInNet(NovartisIP, "86.116.46.0", "255.255.255.0")	||
		 isInNet(NovartisIP, "86.116.50.0", "255.255.255.0")	||
		 isInNet(NovartisIP, "86.116.71.0", "255.255.255.0")	||
		 isInNet(NovartisIP, "86.117.131.0", "255.255.255.0")	||
		 isInNet(NovartisIP, "86.117.132.0", "255.255.252.0")	||
		 isInNet(NovartisIP, "86.117.136.0", "255.255.248.0")	||
		 isInNet(NovartisIP, "86.117.144.0", "255.255.248.0")	||
		 isInNet(NovartisIP, "86.117.204.0", "255.255.252.0")
		 isInNet(NovartisIP, "172.29.224.0", "255.255.254.0")
		))
		return usemproxy;


    // *************************
    // FQDN Exceptions that go DIRECT
    // *************************
	if (isPlainHostName(host) 							||
		dnsDomainIs(host, ".023dapp.com") 				||
		dnsDomainIs(host, ".023d.mgd.msft.net") 		||
		dnsDomainIs(host, ".alconnet.com") 				||
		dnsDomainIs(host, ".alconnet.dev") 				||
		dnsDomainIs(host, ".alconnet.tst") 				||
		dnsDomainIs(host, ".ap.novartis.net") 			||
		dnsDomainIs(host, ".ariba.nn.com") 				||
		dnsDomainIs(host, ".aribaqa.nn.com") 			||
		dnsDomainIs(host, ".aribadev.nn.com") 			||
		dnsDomainIs(host, ".atku") 						||
		dnsDomainIs(host, ".bnp.intra") 				||
		dnsDomainIs(host, ".chbs") 						||
		dnsDomainIs(host, ".chiron.com") 				||
		dnsDomainIs(host, ".chiron-europe.com") 		||
		dnsDomainIs(host, ".chiron-behring.com") 		||
		dnsDomainIs(host, ".chironvaccines.com") 		||
        dnsDomainIs(host, ".clin-pro.com") 				||
		dnsDomainIs(host, ".cobo") 						||
		dnsDomainIs(host, ".defr") 						||
		dnsDomainIs(host, ".degw") 						||
		dnsDomainIs(host, ".dewe") 						||
		dnsDomainIs(host, ".eu.novartis.net")			||
		dnsDomainIs(host, ".excellenceaddsup.com")		||
		dnsDomainIs(host, ".gecis.ge.com")				||
		dnsDomainIs(host, ".genoptix.org")				||
		dnsDomainIs(host, ".genpact.ge.com")			||
		dnsDomainIs(host, ".global.corp.chiron.com")	||
		dnsDomainIs(host, ".gnf.org")					||
		dnsDomainIs(host, ".intra")						||
		dnsDomainIs(host, ".intranet.genpact.com")		||
		dnsDomainIs(host, ".it2cust.com")				||
		dnsDomainIs(host, ".jpta")						||
		dnsDomainIs(host, ".mxmc")						||
		dnsDomainIs(host, ".na.novartis.net")			||
		dnsDomainIs(host, ".nibr.novartis.net")			||
		dnsDomainIs(host, ".novartis.net")				||
		dnsDomainIs(host, ".novartis.intra")			||
		dnsDomainIs(host, ".novartisdiagnostics.com")	||
		dnsDomainIs(host, ".pharma.novartis.intra")		||
		dnsDomainIs(host, ".pharmaserv.de")				||
		dnsDomainIs(host, ".prod.outlook.com")			||
		dnsDomainIs(host, ".rservices.com")				||
		dnsDomainIs(host, ".triaton.com")				||
		dnsDomainIs(host, ".trle")						||
		dnsDomainIs(host, ".twtp")						||
		dnsDomainIs(host, ".usat")						||
		dnsDomainIs(host, ".usbr")						||
		dnsDomainIs(host, ".useh")						||
		dnsDomainIs(host, ".ussu")						||

	// *************************
	// AGSDC.NET
	// *************************
		localHostOrDomainIs(host, "host4.agsdc.net")	||
		localHostOrDomainIs(host, "host54.agsdc.net")	||

	// *************************
	// CHIRON.COM
	// *************************
		localHostOrDomainIs(host, "citrix.chiron.com") ||
		localHostOrDomainIs(host, "vm-ctxprtl-prd.global.corp.chiron.com") ||

	// *************************
	// Genoptix
	// *************************
		localHostOrDomainIs(host, "intranet.genoptix.org") ||
		localHostOrDomainIs(host, "exchange.genoptix.org") ||
		localHostOrDomainIs(host, "genoptix.org") ||

	// *************************
	// GNF.ORG
	// *************************
		localHostOrDomainIs(host, "portal.gnf.org") ||
		localHostOrDomainIs(host, "intranet.gnf.org") ||
		localHostOrDomainIs(host, "ldweb.gnf.org") ||
		localHostOrDomainIs(host, "cheminfo.gnf.org") ||
		localHostOrDomainIs(host, "compdisc.gnf.org") ||

	// *************************
	// JACOBS.COM
	// *************************
		localHostOrDomainIs(host, "jiaf.jacobs.com") ||
		localHostOrDomainIs(host, "jiaftn.jacobs.com") ||
		localHostOrDomainIs(host, "oakapp10.jacobs.com") ||

	// *************************
	// LYNC/Skype for Business/Office 365
	// *************************
		localHostOrDomainIs(host, "meet.novartis.com") ||
		localHostOrDomainIs(host, "dialin.novartis.com") ||
		localHostOrDomainIs(host, "dial.novartis.com") ||
		localHostOrDomainIs(host, "join.novartis.com") ||
		dnsDomainIs(host, ".join.novartis.com") ||
		url.substring(0,40) == "http://lyncdiscoverinternal.novartis.com" ||
		url.substring(0,41) == "https://lyncdiscoverinternal.novartis.com" ||
		url.substring(0,32) == "http://lyncdiscover.novartis.com" ||
		url.substring(0,33) == "https://lyncdiscover.novartis.com" ||
		url.substring(0,45) == "https://am1023dl50we1.023d.dedicated.lync.com" ||
		url.substring(0,45) == "https://ch1023dl50we1.023d.dedicated.lync.com" ||
		url.substring(0,45) == "https://db3023dl50we1.023d.dedicated.lync.com" ||
		url.substring(0,45) == "https://sn1023dl50we1.023d.dedicated.lync.com" ||
		dnsDomainIs(host, ".023d.dedicated.lync.com") ||
		dnsDomainIs(host, "sts.n-ppe.com") ||

	// *************************
	// LIVE MEETING
	// *************************
		localHostOrDomainIs(host, "lmportal.novartis.net") ||

	// *************************
	// nibr.novartis.cbi
	// *************************
		localHostOrDomainIs(host, "nibr.novartis.cbi")		||

	// *************************
	// Novartis.net
	// *************************
		localHostOrDomainIs(host, "eu.novartis.net") ||
		localHostOrDomainIs(host, "phchbs-s3609.eu.novartis.net") ||
		localHostOrDomainIs(host, "vm-trs-prod2.NA.novartis.net") ||
		localHostOrDomainIs(host, "xts.eu.novartis.net") ||
		localHostOrDomainIs(host, "xtsus.na.novartis.net") ||

	// *************************
	// NOVARTIS.COM
	// *************************
		localHostOrDomainIs(host, "anywhere.novartis.com") ||
		localHostOrDomainIs(host, "apps-jp.novartis.com") ||
		localHostOrDomainIs(host, "autodiscover.novartis.com") ||
		localHostOrDomainIs(host, "mail.novartis.com") ||
		localHostOrDomainIs(host, "next-login.novartis.com") ||
		localHostOrDomainIs(host, "next.novartis.com") ||
		localHostOrDomainIs(host, "pki.novartis.com") ||
		localHostOrDomainIs(host, "sipint1.novartis.com") ||
		localHostOrDomainIs(host, "sipint2.novartis.com") ||
		localHostOrDomainIs(host, "sipinternal.novartis.com") ||
		localHostOrDomainIs(host, "st-www.novartis.com") ||
        localHostOrDomainIs(host, "sts.novartis.com") ||

	// *************************
	// NOVARTIS.INTRA
	// *************************
		localHostOrDomainIs(host, ".glosud.novartis.intra") ||

	// *************************
	// PG.COM
	// *************************
		localHostOrDomainIs(host, "mygateway.pg.com") ||
		localHostOrDomainIs(host, "password.pg.com") ||

	// *************************
	// QUINTILES.COM
	// *************************
		localHostOrDomainIs(host, "jreview1.qstr.quintiles.com") ||
		localHostOrDomainIs(host, "portal.qstr.quintiles.com") ||

	// *************************
	// MISC
	// *************************
		localHostOrDomainIs(host, "*procom-deaf.ch") ||
		localHostOrDomainIs(host, "*sipproxy.telesip.ch") ||
		localHostOrDomainIs(host, "ehrms2.adphc.com") ||
		localHostOrDomainIs(host, "hostedgateway.adphc.com") ||
		localHostOrDomainIs(host, "myscience.premier.appliedbiosystems.com") ||
		localHostOrDomainIs(host, "nahus.com") ||
		localHostOrDomainIs(host, "premier.celera.com") ||
		localHostOrDomainIs(host, "premier.appliedbiosystems.com") ||
		localHostOrDomainIs(host, "sdzwebwin001.sdz.at.emea.csc.com") ||
		localHostOrDomainIs(host, "www.bestinclass.intra") ||
		localHostOrDomainIs(host, "www.chinahris.intra") ||

	// *************************
	// .local reserved as last statement of the DIRECT FQDN IF statement
	// *************************
		shExpMatch(host, "*.local")
		)
		return noproxy;


    // *************************
    // IP Address Exceptions that go DIRECT
    // *************************
	if (shExpMatch(host, "10.*") 						||
		shExpMatch(host, "172.16.*") 					||
		shExpMatch(host, "172.17.*") 					||
		shExpMatch(host, "172.18.*") 					||
		shExpMatch(host, "172.19.*") 					||
		shExpMatch(host, "172.20.*") 					||
		shExpMatch(host, "172.21.*") 					||
		shExpMatch(host, "172.22.*") 					||
		shExpMatch(host, "172.23.*") 					||
		shExpMatch(host, "172.24.*") 					||
		shExpMatch(host, "172.25.*") 					||
		shExpMatch(host, "172.26.*") 					||
		shExpMatch(host, "172.27.*") 					||
		shExpMatch(host, "172.28.*") 					||
		shExpMatch(host, "172.29.*") 					||
		shExpMatch(host, "172.30.*") 					||
		shExpMatch(host, "172.31.*") 					||
		shExpMatch(host, "192.168.*") 					||
		shExpMatch(host, "8.20.169.55") 				||
		shExpMatch(host, "8.20.169.56") 				||
		shExpMatch(host, "63.111.62.135") 				||
		shExpMatch(host, "86.116.*") 					||
		shExpMatch(host, "86.117.*") 					||
		shExpMatch(host, "94.245.113.*") 				||
		shExpMatch(host, "135.89.*") 					||
		shExpMatch(host, "141.251.*") 					||
		shExpMatch(host, "147.167.*") 					||
		shExpMatch(host, "147.204.*") 					||
		shExpMatch(host, "159.32.*") 					||
		shExpMatch(host, "160.61.*") 					||
		shExpMatch(host, "160.62.*") 					||
		shExpMatch(host, "160.86.*") 					||
		shExpMatch(host, "161.61.*") 					||
		shExpMatch(host, "161.108.*") 					||
		shExpMatch(host, "162.86.*") 					||
		shExpMatch(host, "165.140.*") 					||
		shExpMatch(host, "167.210.168.215")				||
		shExpMatch(host, "170.27.114.*") 				||
		shExpMatch(host, "170.60.*") 					||
		shExpMatch(host, "170.236.*") 					||
		shExpMatch(host, "170.237.*") 					||
		shExpMatch(host, "191.234.128.*") 				||
		shExpMatch(host, "191.234.129.*") 				||
		shExpMatch(host, "191.234.130.*") 				||
		shExpMatch(host, "191.234.131.*") 				||
		shExpMatch(host, "191.234.132.*") 				||
		shExpMatch(host, "191.234.133.*") 				||
		shExpMatch(host, "191.234.134.*") 				||
		shExpMatch(host, "191.234.135.*") 				||
		shExpMatch(host, "191.235.0.*") 				||
		shExpMatch(host, "191.235.1.*") 				||
		shExpMatch(host, "191.235.2.*") 				||
		shExpMatch(host, "191.235.3.*") 				||
		shExpMatch(host, "191.235.4.*") 				||
		shExpMatch(host, "191.235.5.*") 				||
		shExpMatch(host, "191.235.6.*") 				||
		shExpMatch(host, "191.235.7.*") 				||
		shExpMatch(host, "191.235.8.*") 				||
		shExpMatch(host, "191.235.9.*") 				||
		shExpMatch(host, "191.235.10.*") 				||
		shExpMatch(host, "191.235.11.*") 				||
		shExpMatch(host, "191.235.12.*") 				||
		shExpMatch(host, "191.235.13.*") 				||
		shExpMatch(host, "191.235.14.*") 				||
		shExpMatch(host, "191.235.15.*") 				||
		shExpMatch(host, "192.37.*") 					||
		shExpMatch(host, "192.135.*") 					||
		shExpMatch(host, "193.67.12.*") 				||
		shExpMatch(host, "194.209.105.95") 				||
		shExpMatch(host, "195.203.118.*") 				||
		shExpMatch(host, "200.240.*") 					||
		shExpMatch(host, "202.236.*") 					||
		shExpMatch(host, "205.181.102.*") 				||
		shExpMatch(host, "216.13.208.*") 				||
	// loopback 127.0.0.0/8 reserved as last statement of the RIRECT IP Address IF statement
		shExpMatch(host, "127.0.*")
		)
		return noproxy;


	//  Default rule

		return nibrproxy;
}

